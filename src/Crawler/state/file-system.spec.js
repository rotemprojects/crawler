'use strict';

const chai = require('chai')
const expect = chai.expect;
const sinon = require('sinon')
const sinonChai = require('sinon-chai')

const FSState = require('./file-system')

describe("FSCrawlerState", function() {

  describe("Init", function() {
    let state = new FSState();
    before(function*() {
      yield state.init('hello.com', 1)
      yield state.dequeueJobs()
      yield state.init('hello.com', 1)
    })
    after(function *() {
      yield state.tearDown()
    })

    it('should load the in flight messages back to the queue', function() {
      expect(state._state.queue.length).to.eql(1)
    })
  })

  describe("Dequeue", function() {
    let state = new FSState();
    before(function*() {
      yield state.init('hello.com', 1)
      yield state.enqueueJob('bye.com', 0)
      yield state.enqueueJob('shalom.com', 0)
      yield state.dequeueJobs(10)
    })
    after(function *() {
      yield state.tearDown()
    })

    it('should empty the queue', function() {
      expect(state._state.queue.length).to.eql(0)
    })

    it('should place the messages into in flight', function() {
      expect(state._state.in_flight.length).to.eql(3)
    })
  })

  describe('Clear in flight', function() {
    let state = new FSState();
    before(function*() {
      yield state.init('hello.com', 1)
      yield state.enqueueJob('bye.com', 0)
      yield state.enqueueJob('shalom.com', 0)
      yield state.dequeueJobs(10)
      yield state.clearInFlight()
    })
    after(function *() {
      yield state.tearDown()
    })

    it('should clear the messages into in flight', function() {
      expect(state._state.in_flight.length).to.eql(0)
    })

  })



})