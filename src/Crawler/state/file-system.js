'use strict';

const fs = require('fs-promise')

module.exports = class FSCrawlerState {
  constructor(opts = {stateFile: '.crawler.json'}) {
    this._stateFile = opts.stateFile
    this._state = {
      queue: [],
      visited: {},
      results: [],
      in_flight: []
    }
  }

  *init(rootUrl, maxDepth) {
    yield fs.ensureFile(this._stateFile)
    try {
      this._state = (yield fs.readJson(this._stateFile))
      if(this._state.in_flight.length > 0)
        yield this._reqEnqeueInflight()
    } catch (err) {
      yield this.enqueueJob(rootUrl, maxDepth)
    }
  }

  *tearDown() {
    yield fs.remove(this._stateFile)
  }

  *clearInFlight() {
    this._state.in_flight = []
    yield this._persistState()
  }

  *_persistState() {
      yield fs.writeJson(this._stateFile, this._state)
  }

  *_reqEnqeueInflight() {
    this._state.queue = [...this._state.in_flight, ...this._state.queue]
    yield this._persistState()
  }

  *enqueueJob(url, maxDepth) {
    this._state.queue.push({url: url, maxDepth: maxDepth})
    yield this._persistState()
  }

  *dequeueJobs(n) {
    let jobs = this._state.queue.splice(0, n)
    this._state.in_flight = [...this._state.in_flight, ...jobs]
    yield this._persistState()
    return jobs
  }

  *getJobQueueSize() {
    return this._state.queue.length
  }

  *setVisited(url) {
    this._state.visited[url] = true
    yield this._persistState()
  }

  *getVisited(url) {
    return this._state.visited[url]
  }

  *getResults() {
    return this._state.results
  }

  *recordResult(url, depth, ratio) {
    this._state.results.push({
      url: url,
      depth: depth,
      ratio: ratio
    })
    yield this._persistState()
  }
}



