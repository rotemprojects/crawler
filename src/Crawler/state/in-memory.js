'use strict';

const fs = require('fs-promise')

module.exports = class InMemoryCrawlerState {
  constructor() {
    this._queue = []
    this._visited = {}
    this._results = []
  }

  *init(rootUrl, maxDepth) {
    yield this.enqueueJob(rootUrl, maxDepth)
  }

  *tearDown() {

  }

  *clearInFlight() {

  }

  *enqueueJob(url, maxDepth) {
    this._queue.push({url: url, maxDepth: maxDepth})
  }

  *dequeueJobs(n) {
    return this._queue.splice(0, n)
  }

  *getJobQueueSize() {
    return this._queue.length
  }

  *setVisited(url) {
    this._visited[url] = true
  }

  *getVisited(url) {
    return this._visited[url]
  }

  *getResults() {
    return this._results
  }

  *recordResult(url, depth, ratio) {
    this._results.push({
      url: url,
      depth: depth,
      ratio: ratio
    })
  }
}