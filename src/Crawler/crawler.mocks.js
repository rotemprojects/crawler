'use strict';

module.exports = {
  RESPONSE_0: {
    headers: {
      'content-type': 'text/html',
    },
    data: `<html>
        <a href="http://github.com/rotemtam">My Github</a>
        <a href="http://facebook.com/pixel.gif">My Github</a>
        </html>`
  },
  RESPONSE_1: {
    headers: {
      'content-type': 'text/html',
    },
    data: `<html>This is github.com. <a href="http://google.com">Google</a></html>`
  },
  RESPONSE_2: {
    headers: {
      'content-type': 'image/gif',
    },
    data: `<raw data>`
  },
  RESPONSE_3: {
    headers: {
      'content-type': 'text/html',
    },
    data: `<html>This is google.com, with a
<a href="http://rotemtam.com>">Recursive Link to the Top</a>
<a href="http://google.com">And a link to self</a></html>`
  }
}