'use strict';

const urllib = require('url')
const getLinks = require('../lib/get-links')
const removeHash = require('../lib/remove-hash')

module.exports = class Crawler {

  constructor(services, config = {}) {
    this.state = services.state
    this.httpGetter = services.httpGetter
    this.evaluator = services.evaluator
    this.outputter = services.outputter
    this.max_concurrent_fetches = config.max_concurrent_fetches || 100
  }

  *fetch(url, maxDepth=10) {
    this._jobDepth = maxDepth
    let i = 0;
    yield this.state.init(url, maxDepth)

    while(yield this.consumeQueue()) {
      console.log(`Consuming backlog queue. Have ${yield this.state.getJobQueueSize()} pending messages`)
      yield this.state.clearInFlight()
    }
    yield this.outputter.write(yield this.state.getResults())

    yield this.state.tearDown()

    return this.results
  }

  *consumeQueue() {
    let jobs = yield this.state.dequeueJobs(this.max_concurrent_fetches)
    jobs = jobs.map( (job) => this._process(job.url, job.maxDepth))
    yield jobs
    return (yield this.state.getJobQueueSize());
  }



  *_process(url, maxDepth) {
    console.log('Processing ', url)
    if(maxDepth == 0) {
      console.log('Reached max depth, skipping.')
      return
    } else if ((yield this.state.getVisited(url))) {
      console.log('Already visited. Skipping.')
      return
    }


    let page;
    try {
      page = yield this.httpGetter.get(url)
    } catch(err) {
      console.error(`failed fetching ${url}, err: ${err.message}` )
    }

    yield this.state.setVisited(url)

    if(!page)
      return

    if (! /text\/html/.test(page.mimeType)) {
      console.log(url, ': Response isnt a text/html file. Skipping', page.mimeType)
      return;
    }

    let ratio = this.evaluator.evaluate(url, page.body)
    console.log(`${url} score: ${ratio}`)
    yield this.state.recordResult(url, this._jobDepth - maxDepth + 1, ratio)

    if ( maxDepth==1 )
      return;

    let pageLinks = getLinks(page.body)
      .map(removeHash)
      .map( (link) => urllib.resolve(url, link))

    // dedupe:
    pageLinks = [...new Set(pageLinks)]

    for(let link of pageLinks) {
      yield this.state.enqueueJob(link, maxDepth - 1)
    }

  }


};
