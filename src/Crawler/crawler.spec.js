'use strict';

const sinon = require('sinon')
const expect = require('chai').expect

const request = require('axios')
const fs = require('fs-promise')
const Crawler = require('./')
const InMemCrawlerState = require('./state/in-memory')
const InMemPageCache = require('../http-getter/cache/in-memory')
const TSVWriter = require('../outputter/tsv-file-outputter')
const PageEvaluator = require('../page-evaluator')
const HttpGetter = require('../http-getter')

const MOCKS = require('./crawler.mocks')

require('co-mocha');
require('sinon-as-promised');

describe('Crawler', function()  {
  this.timeout(20000)

  describe('fetch', function() {
    let fetcher, res;

    before(function() {
      let stub = sinon.stub(request, 'get')
      stub.onCall(0).resolves(MOCKS.RESPONSE_0)
      stub.onCall(1).resolves(MOCKS.RESPONSE_1)
      stub.onCall(2).resolves(MOCKS.RESPONSE_2)
      stub.onCall(3).resolves(MOCKS.RESPONSE_3)

      sinon.stub(TSVWriter.prototype, 'write').resolves()
    })

    after(function*() {
      request.get.restore()
      TSVWriter.prototype.write.restore()
    })

    before(function*() {
      let pageCache = new InMemPageCache()
      fetcher = new Crawler({
        state: new InMemCrawlerState(),
        httpGetter: new HttpGetter({cache: pageCache}),
        evaluator: new PageEvaluator(),
        outputter: new TSVWriter('crawl-out.tsv')
      })
      res = yield fetcher.fetch('http://rotemtam.com', 3)
    })

    after(function *() {
      yield fs.remove('crawl-out.tsv')
    })

    it('should make 4 http requests', function() {
      expect(request.get.callCount).to.eql(4)
    })

    it('should record all results to the outputter', function() {
      expect(TSVWriter.prototype.write).calledWith([
        { depth: 1, ratio: 0, url: "http://rotemtam.com" },
        { depth: 2, ratio: 0, url: "http://github.com/rotemtam" },
        { depth: 3, ratio: 0.5, url: "http://google.com/" }
      ])
    })

  })
});
