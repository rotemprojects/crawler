const cheerio = require('cheerio')

module.exports = function getLinks(body) {
  let $ = cheerio.load(body)
  return $('a').map( (_, link) => link.attribs.href).get()
}