'use strict';

const urllib = require('url')

module.exports = function(fullUrl) {
  let parsed = urllib.parse(fullUrl)
  if(!parsed.hash)
    return fullUrl

  return fullUrl.replace(parsed.hash, '')
}