'use strict';

const fs = require('fs-promise')

const HEADERS = ['url', 'depth', 'ratio']

module.exports = class TSVFileOutputter {
  constructor(fileLocation) {
    this.data = []
    this.fileLocation = fileLocation
  }


  _stringifyRecord(r) {
    return `${r.url}\t${r.depth}\t${r.ratio}`
  }

  *write(data) {
    let doc = `url\tdepth\tratio\t
${data.map(this._stringifyRecord).join('\n')}`
    yield fs.outputFile(this.fileLocation, doc)
  }
}