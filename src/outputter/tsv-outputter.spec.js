'use strict';

const expect = require('chai').expect
const fs = require('fs-promise')
const TSVOutputter = require('./tsv-file-outputter')

const PATH = 'out.tsv'

describe('TSV Outputter', function() {
  let output;
  before(function *() {
    let writer = new TSVOutputter(PATH)

    yield writer.write([
      {url: 'http://google.com', depth: 1, ratio: 0.7}
    ])

    output = (yield fs.readFile(PATH)).toString('utf-8')
  })
  after(function *() {
    yield fs.remove(PATH)
  })

  it('should write a tsv file', function() {
    expect(output).to.eql(`url\tdepth\tratio\t\nhttp://google.com\t1\t0.7`)
  })


})
