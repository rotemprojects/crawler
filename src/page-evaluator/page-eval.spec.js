'use strict';
const expect = require('chai').expect
const PageEvaluator = require('./')

const URL = `http://www.foo.com/bar.html`
const DOCUMENT = `
<a href="http://www.foo.com/a.html">Link</a>

<a href="http://www.foo.com/b/c.html">Link</a>

<a href="http://baz.foo.com/">Link</a>

<a href="http://www.google.com/baz.html">Link</a>

<a href="/same/domain/link">A link to the same domain</a>

<a href="#hello">A link to the page (same domain as well)</a>

`
describe('Page Evaluator', function() {

  let res, evaluator = new PageEvaluator();
  before(function() {
    res = evaluator.evaluate(URL,DOCUMENT)
  })

  it('should calculate the same link ratio', function() {
    expect(res).to.eql(4 / 6)
  })

})