'use strict';

const urllib = require('url')
const getLinks = require('../lib/get-links')

module.exports = class PageEvaluator {
  constructor() {}

  evaluate(url, body) {
    let domain = this._getDomain(url)
    let links = getLinks(body)
    links = links.map( (link) => urllib.resolve(url, link))
    let sameDomainLinks = links.filter( (link) => this._getDomain(link) == domain  )
    return links.length ? (sameDomainLinks.length / links.length) : 0
  }

  _getDomain(fullUrl) {
    return urllib.parse(fullUrl).hostname
  }

}