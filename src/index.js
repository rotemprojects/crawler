'use strict';

const program = require('commander')

const Crawler = require('./Crawler')
const CrawlerState = require('./Crawler/state/file-system')
const FSPageCache = require('./http-getter/cache/file-system')
const TSVWriter = require('./outputter/tsv-file-outputter')
const PageEvaluator = require('./page-evaluator')
const HttpGetter = require('./http-getter')

const co = require('co')


function *main(url, maxDepth) {
  let pageCache = new FSPageCache()
  let crawler = new Crawler({
    state: new CrawlerState(),
    httpGetter: new HttpGetter({cache: pageCache}, {timeout: 3000, maxContentLength: 1024*256}),
    evaluator: new PageEvaluator(),
    outputter: new TSVWriter('crawl-out.tsv')
  })
  console.log(yield crawler.fetch(url, maxDepth))
}


program
  .version('0.0.1')
  .arguments('<url> <maxDepth>')
  .action(function (url, maxDepth) {
    co(function*() {
      yield main(url, maxDepth)
    })
  });

program.parse(process.argv);
