'use strict';

const chai = require('chai')
const expect = chai.expect;
const sinon = require('sinon')
const sinonChai = require('sinon-chai')

const FSCache = require('./file-system')

const URL = 'http://rotemtam.com'
const DATA = {
  body: 'hello world',
  etag: '1234'
}

// TODO: test loading state from file

describe('File System Page Cache', function() {

  let cache, res;
  before(function *() {
    cache = new FSCache()
    yield cache.init()
    yield cache.set(URL, DATA)
    res = yield cache.get(URL)
  })

  after(function *() {
    yield cache.tearDown()
  })

  it('Sets and Gets an entry', function() {
    expect(res).to.eql(DATA)
  })

})