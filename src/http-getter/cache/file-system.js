'use strict';

const sanitizeFilename = require("sanitize-filename")
const fs = require('fs-promise')
const storage = require('node-persist')


module.exports = class FileSystemPageCache {
  constructor(opts) {
    this._opts = opts || {pagesDir: '.pages'}
    this._state = {}
    this._index_file = `${this._opts.pagesDir}/index.db`
  }

  *init() {
    yield fs.ensureDir(this._opts.pagesDir)
    yield fs.ensureFile(this._index_file)
    try {
      this._state = (yield fs.readJson(this._index_file))
    } catch(err) {
      this._state = {}
    }
  }

  *tearDown() {
    yield fs.emptyDir(this._opts.pagesDir)
  }

  *set(key, val) {
    let fileLocation = `${this._opts.pagesDir}/${this.cleanUrl(key)}.html`
    yield fs.outputFile(fileLocation, val.body)

    val = Object.assign({}, val, {fileLocation: fileLocation})
    delete val.body
    this._state[key] = val
    yield this._persistState()
  }

  *_persistState() {
    yield fs.writeJson(this._index_file, this._state)
  }

  *get(key) {
    let entry = this._state[key]
    if(!entry)
      return;
    let body = yield fs.readFile(entry.fileLocation)
    entry = Object.assign({}, entry, {body: body.toString('utf-8')})
    delete entry.fileLocation
    return entry
  }

  cleanUrl(url) {
    return sanitizeFilename(url, {replacement: '-'})
  }

}