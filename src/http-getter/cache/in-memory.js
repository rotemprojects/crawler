'use strict';

module.exports = class InMemoryPageCache {
  constructor() {
    this.items = {}
  }

  init() {
    return Promise.resolve()
  }

  *set(key, val) {
    this.items[key] = val
  }

  *get(key) {
    return Promise.resolve(this.items[key])
  }
}