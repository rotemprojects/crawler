'use strict';

const request = require('axios')

const NO_PROTOCOL_PREFIX_REGEX = /^\/\//

module.exports = class HttpGetter {

  constructor(services, opts = {timeout: 3000, maxContentLength: 1024 * 512}) {
    this.cache = services.cache
    this._initialized = false
    this._timeout = opts.timeout
    this._maxContentLength = opts.maxContentLength
  }

  *get(url) {
    if(!this._initialized)
      yield this.cache.init()
    url = this._addProtocol(url)

    let cachedEntry = yield this.cache.get(url)

    let headers = {}
    if(cachedEntry && cachedEntry.etag) {
      headers['If-None-Match'] = cachedEntry.etag
    }

    let resp;

    try {
      resp = yield request.get(url, {
        headers: headers,
        timeout: this._timeout,
        maxContentLength: this._maxContentLength
      })
    } catch(err) {
      if(err.response && err.response.status == 304) {
        resp = err.response
      } else {
        throw err
      }
    }


    if (resp.status == 304) {
      console.log('cache hit for ', url)
      return cachedEntry
    }

    let page = this._extractPageFields(resp)

    yield this.cache.set(url, page)

    return page
  }


  _extractPageFields(httpResponse) {
    return  {
      mimeType: httpResponse.headers['content-type'],
      body: httpResponse.data,
      etag: httpResponse.headers['etag']
    };
  }

  _addProtocol(url) {
    if(NO_PROTOCOL_PREFIX_REGEX.test(url))
      return `http:${url}`
    return url
  }

};
