'use strict';
const chai = require('chai')
const expect = chai.expect;
const sinon = require('sinon')
const sinonChai = require('sinon-chai')

chai.use(sinonChai)

const request = require('axios');
const InMemCache = require('./cache/in-memory');
const HttpGetter = require('./');

require('co-mocha')
require('sinon-as-promised')

const URL = 'http://rotemtam.com'

const MOCK_AXIOS_RESPONSE = {
  headers: {
    'content-type': 'text/html',
    'etag': 'abcdefg'
  },
  data: `<html><a href="http://github.com/rotemtam">My Github</a></html>`
}

const MOCK_AXIOS_RESPONSE_2 = {
  headers: {
    'content-type': 'text/html',
    'etag': 'xyz'
  },
  data: `<html><a href="http://github.com/rotemtam">My Github Page</a></html>`
}

describe('httpGetter', function()  {
  this.timeout(200000)

  describe('When page doesnt exist in cache yet', function() {
    describe('get response', () => {

      let getter = new HttpGetter({cache: new InMemCache()}), res;

      before(function() {
        sinon.stub(request, 'get').returns( Promise.resolve(MOCK_AXIOS_RESPONSE) )
      });
      after(function() {
        request.get.restore()
      })
      before(function *() {
        res = yield getter.get('http://rotemtam.com')
      });

      it('should have a mime type', () => {
        expect(res.mimeType).to.eql('text/html');
      });

      it('should have string body', () => {
        expect(res.body).to.be.a('String')
      });

      it('should have a cache key for the retrieved domain afterwards', function*() {
        expect(yield getter.cache.get('http://rotemtam.com')).to.be.ok
      })
    });


  })

  describe('When page exists in cache before', function() {
    describe('and the server returns 304 Not Changed', () => {

      let getter = new HttpGetter({cache: new InMemCache()}), res;

      before(function() {
        sinon.stub(request, 'get').resolves({
          status: 304
        })
      });
      after(function() {
        request.get.restore()
      })
      before(function *() {
        yield getter.cache.set(getter._addProtocol(URL), getter._extractPageFields(MOCK_AXIOS_RESPONSE))
        res = yield getter.get(URL)
      });

      it('should have a mime type', () => {
        expect(res.mimeType).to.eql('text/html');
      });

      it('should have string body', () => {
        expect(res.body).to.be.a('String')
      });

      it('should make the request with the original etag headers', function() {
        expect(request.get).to.have.been.calledWith(URL, {
          headers: {'If-None-Match': MOCK_AXIOS_RESPONSE.headers.etag},
          timeout: 3000,
          maxContentLength: 524288
        })
      })

      it('should have a cache key for the retrieved domain afterwards', function*() {
        expect(yield getter.cache.get('http://rotemtam.com')).to.be.ok
      })

    });
    describe('and the server returns 200', () => {

      let getter = new HttpGetter({cache: new InMemCache()}), res, cacheEntry;

      before(function() {
        sinon.stub(request, 'get').resolves(MOCK_AXIOS_RESPONSE_2)
      });
      after(function() {
        request.get.restore()
      })
      before(function *() {
        const URL = 'http://rotemtam.com'
        yield getter.cache.set(getter._addProtocol(URL), getter._extractPageFields(MOCK_AXIOS_RESPONSE))
        res = yield getter.get(URL)
        cacheEntry = yield getter.cache.get('http://rotemtam.com')
      });

      it('should have the new body', () => {
        expect(res.body).to.match(/My Github Page/)
      });

      it('should have update the cache entry', function*() {
        expect(cacheEntry.etag).to.eql(MOCK_AXIOS_RESPONSE_2.headers.etag)
      })

    });
  });

});
