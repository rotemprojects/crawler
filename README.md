# Web Crawler

Crawls the web and calculates same-domain links.

### Installation

*requirements: node 6.9.5*

```bash

npm install

```

### Running

Usage: `npm run crawl <url> <maxDepth>`
```bash
npm run crawl http://rotemtam.com 3

```

Results will be written in TSV format to `crawl-out.tsv`
```bash
cat crawl-out.tsv
```


### Run Unit Tests

```bash
npm test
```